from typing import Iterable

from src.config.config import RESULT_PATH, config
from src.extract import NotFoundRequiredColumnsError, extract
from src.prepare_sql import prepare
from src.save import save_to_file
from src.source import GetDataError, get_city_data
from src.types import CityRow


def main() -> None:
    if (raw_cities := _get_cities(config["source"]["url"])) is None:
        return

    if (clean_cities := _extract_required_columns(raw_cities)) is None:
        return

    _save_result(_prepare_data_to_save(clean_cities))


def _get_cities(url: str) -> str | None:
    try:
        print("1. Started to receive data of cities.")
        return get_city_data(url)
    except GetDataError as exc:
        print(f"-- ERROR: {exc.message}")
        return None


def _extract_required_columns(source: str) -> Iterable[CityRow] | None:
    try:
        print("2. Started extracting the required data of cities.")
        return extract(source, columns=config["destination"]["from_columns"])
    except NotFoundRequiredColumnsError as exc:
        print(f"-- ERROR: {exc.message}")
        return


def _prepare_data_to_save(cities: Iterable[CityRow]) -> Iterable[str]:
    print("3. Started preparing SQL queries.")
    return prepare(cities, config["destination"]["table_name"])


def _save_result(sql_queries: Iterable[str]) -> None:
    print("4. Started save SQL queries to a file.")
    save_to_file(RESULT_PATH, sql_queries)


if __name__ == "__main__":
    main()
