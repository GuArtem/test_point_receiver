# test_point_receiver

This script is designed to extract title values and coordinates of Russian cities from a https://github.com/hflabs/city.
Generates a SQL script with test data for the 'route_planning' project.

## Installation
Install Poetry and clone the project.

## Usage
> Notice: Poetry is required to use this script.

You can change the settings in the `src/config/config.yml`.

Create environment ([poetry docs](https://python-poetry.org/docs/managing-environments/)):
```bash
$ poetry env use <full path to python>
```
Install requirements:
```bash
$ poetry install
```
And run:
```bash
$ python main.py
```
The script will create a "result.sql" file in the root directory.
The result will contain SQL of the format:
```sql
INSERT INTO <table name> (<title col>, <coordinates col>) VALUES ('<city name>', 'POINT(<lat> <lon>)'), ...;
```

> Warning: City names will not be validated in the resulting SQL. You must check them yourself.

## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
