from typing import NamedTuple


class CityRow(NamedTuple):
    title: str
    lat: str
    lon: str
