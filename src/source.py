import requests


class GetDataError(Exception):
    message = "Failed to get data from remote source."


def get_city_data(url: str) -> str:
    response: requests.Response = requests.get(url)
    if response.status_code != 200:
        raise GetDataError
    return response.text
