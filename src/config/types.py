from typing import TypedDict


class SourceConfig(TypedDict):
    url: str


class DestinationConfig(TypedDict):
    from_columns: str
    to_columns: str
    table_name: str


class Config(TypedDict):
    source: SourceConfig
    destination: DestinationConfig
