from pathlib import Path

import yaml

from src.config.types import Config

BASE_DIR = Path(__file__).parent.parent.parent
CONFIG_PATH = BASE_DIR / "src" / "config" / "config.yml"
RESULT_PATH = BASE_DIR / "result.sql"


def _load_config(path: Path) -> Config:
    with open(path) as conf_file:
        return yaml.safe_load(conf_file)


config: Config = _load_config(CONFIG_PATH)
