import os
from typing import Iterable

from src.config.config import config
from src.types import CityRow
from src.validate import is_valid

BASE_QUERY_TEMPL = "INSERT INTO {table_name!s} ({columns!s}) VALUES {values!s};"
QUERY_VALUE_TEMPL = "('{title!s}', 'POINT({lat!s} {lon!s})')"


def prepare(cities: Iterable[CityRow], table_name: str) -> Iterable[str]:
    sql_values: list[str] = []
    for row_num, city in enumerate(cities):
        if not is_valid(city):
            log_text = "invalid and exclude city data"
            print(f"--- VALIDATION ERROR: {log_text}: {row_num}. - {city}")
            continue
        query_value = QUERY_VALUE_TEMPL.format(
            title=city.title,
            lat=city.lat,
            lon=city.lon,
        )
        sql_values.append(query_value)

        if row_num % 100 == 0 and row_num != 0:
            yield _prepare_full_query(table_name, sql_values)
            sql_values = []

    if len(sql_values) > 0:
        yield _prepare_full_query(table_name, sql_values)


def _prepare_full_query(table_name: str, sql_values: list[str]) -> str:
    query = BASE_QUERY_TEMPL.format(
        table_name=table_name,
        values=", ".join(sql_values),
        columns=get_destination_columns(config["destination"]["to_columns"]),
    )
    return f"{query}{os.linesep}"


def get_destination_columns(to_columns: str) -> str:
    return ", ".join(to_columns.split(","))
