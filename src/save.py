from pathlib import Path
from typing import Iterable


def save_to_file(file_path: Path, sql_queries: Iterable[str]) -> None:
    with open(file_path, mode="w", encoding="utf-8") as result_file:
        result_file.writelines(sql_queries)
