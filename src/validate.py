from src.types import CityRow


class BaseValidator:
    min: int = 0
    max: int = 0

    def __init__(self, value: str):
        self._value = value

    def is_valid(self) -> bool:
        try:
            value = float(self._value)
        except ValueError:
            return False

        return self.min < value < self.max


class LatValidator(BaseValidator):
    min: int = -90
    max: int = 90


class LonValidator(BaseValidator):
    min: int = -180
    max: int = 180


def is_valid(city: CityRow) -> bool:
    lat_validator = LatValidator(city.lat)
    lon_validator = LonValidator(city.lon)

    return lat_validator.is_valid() and lon_validator.is_valid()


