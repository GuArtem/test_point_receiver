import csv
from io import StringIO
from typing import Iterable, OrderedDict, TypeAlias

from src.types import CityRow

RawCityRow: TypeAlias = OrderedDict[str, str]


class NotFoundRequiredColumnsError(Exception):
    message = "No required column found"


def extract(raw_value: str, columns: str) -> Iterable[CityRow]:
    csv_reader = csv.DictReader(StringIO(raw_value))
    for row in csv_reader:
        yield extract_required(
            city=row,
            from_columns=columns,
        )


def extract_required(city: RawCityRow, from_columns: str) -> CityRow:
    title, lat, lon = from_columns.split(",")
    try:
        return CityRow(
            title=city[title],
            lat=city[lat],
            lon=city[lon],
        )
    except KeyError:
        raise NotFoundRequiredColumnsError
